# Wardrobify

Team:

* Person 1 - Jonathan is doing shoes
* Person 2 - Corinne is doing hats

## Design

## Shoes microservice

The shoe model uses a many to one relationship with bin. As we can have more than one shoe in a bin. We poll the data from the wardrobe api which has the bin list. We use the href of the binVO to file each shoe to each bin. We do this by making the href value always equal the specific BinVO.

## Hats microservice

I've developed two models, a LocationVO model and a Hat model, that contain all the necessary information to create each respective object. To connect a location to a hat object, I've created a location encoder, a HatList encoder that only takes the closet name from location, and a HatDetail encoder that takes all information from location.

Next, I created two view functions: 'List Hats', which supports both GET and POST requests, and 'Show Hat', which supports both GET and DELETE requests. I've also registered both Hat and LocationVO models to my admin.py file and created URL paths for both view functions.

Moving on to React, I've created a "HatList" JavaScript file that fetches data from the URL path created earlier and displays it in a table. I've also added a "remove" function that allows users to delete a specific hat by clicking a button included in the table.

Next, I worked on my "NewHat" JavaScript file, where I used a fetch request to my locations API to connect a new hat to its respective location. I added change events for each piece of data, and a handle submit event to save the information. Upon submission, I included the POST request below and set all boxes to be blank. I then created a form HTML with the appropriate change event in each input and a drop-down selection for location.

Finally, I added routes to both hatlist and newhat in App.js, and modified Nav.js to include an option for users to create a new hat.
