from django.db import models
# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    #bin_number = models.PositiveIntegerField(null=True)
    #bin_size = models.PositiveIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

class Shoe(models.Model):
    shoe_brand = models.CharField(max_length=200)
    shoe_style = models.CharField(max_length=200)
    shoe_color = models.CharField(max_length=200)
    shoe_pic_url = models.URLField(max_length=1000, null=True)
    bin = models.ForeignKey(BinVO, related_name="shoe", null=True, on_delete=models.CASCADE)
