from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.http import JsonResponse
# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        #"bin_number",
        #"bin_size",
        "import_href",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "shoe_brand",
        "shoe_style",
        "shoe_color",
        "shoe_pic_url",
        "bin",
    ]

    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoesListEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            bin_id = content["bin"]
            # print(bin_id)
            bin = BinVO.objects.get(id=bin_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin ID"},
                status=400,
            )

        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoesListEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _= Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
