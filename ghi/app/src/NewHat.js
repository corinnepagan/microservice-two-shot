import React, {useEffect, useState} from 'react';

function NewHat(props) {
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
        else{
            console.error(response)
        }
    }
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.fabric = fabric;
        data.color = color;
        data.location = location;
        data.picture = picture;
        console.log(data);


        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok){
            const data = await response.json();
            console.log(data);

            setName('');
            setFabric('');
            setColor('');
            setLocation('');
            setPicture('');
        }
    }
    useEffect(() => {
        fetchData()
    }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} placeholder="Fabric" required type="text" value={fabric} name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required type="text" id="color" value={color} name="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} placeholder="Picture" required type="img" id="picture" value={picture} name="picture" className="form-control"/>
                <label htmlFor="picture">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" value={location} name="location" className="form-select">
                  <option value=''>Choose Closet</option>
                  {locations.map(location => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                    </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewHat;
