import React, {useEffect, useState} from 'react';
function HatList() {
    const [hatList, setHatList] = useState([])
    async function loadHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        if(response.ok) {
            const data = await response.json();
            setHatList(data.hats);
        }
        else {
            console.error(response);
        }
    }


    useEffect(() => {
        loadHats();
    }, []);

    async function remove(href){
        const fetchConfig = {
            method: "delete"
        };
        const response = await fetch(`http://localhost:8090${href}`, fetchConfig);
        if(response.ok) {
            loadHats();
        }
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {hatList.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.name }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location }</td>
                            <td>
                                <img src={hat.picture} height="50" width="50"></img>
                            </td>
                            <td>
                            <button type="button" className="btn btn-danger" onClick={() => {remove(hat.href)}}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );

}
export default HatList;
