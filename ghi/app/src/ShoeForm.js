import React, {useState, useEffect} from 'react'
import { Link } from "react-router-dom";
function ShoeForm({ setShoes }) {

    const [shoe_brand, setShoeBrand] = useState('')
    const [shoe_style, setShoeStyle] = useState('')
    const [shoe_color, setShoeColor] = useState('')
    const [shoe_pic_url, setShoePicUrl] = useState('')
    const [bin, setBin] = useState('')
    const [bins, setBins] = useState([])

    useEffect(() => {
        async function getBins() {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);

            if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
            console.log(data.bins)
            }
        }
        getBins();
        }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
        shoe_brand,
        shoe_style,
        shoe_color,
        shoe_pic_url,
        bin: bin,
    };


    const locationUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);
        setShoeBrand('');
        setShoeStyle('');
        setShoeColor('');
        setShoePicUrl('');
        setBin('');
        }
    }


    function handleShoeBrand(event) {
        const value = event.target.value;
        setShoeBrand(value);
        }

    function handleShoeStyle(event) {
        const value = event.target.value;
        setShoeStyle(value);
    }

    function handleShoeColor(event) {
        const value = event.target.value;
        setShoeColor(value);
    }

    function handleShoePicUrl(event) {
        const value = event.target.value;
        setShoePicUrl(value);
    }

    function handleBins(event) {
        const value = event.target.value;
        setBin(value);
    }

    return (
        <>
            <div>
                <Link to="/shoes/new" className= "btn-primary mb-3">Create Shoe </Link>
            </div>
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create New Shoes</h1>
                <form onSubmit={handleSubmit} id="create-shoes-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleShoeBrand} value={shoe_brand} placeholder="ShoeBrand" required type="text" name="ShoeBrand" id="ShoeBrand" className="form-control" />
                    <label htmlFor="ShoeBrand">ShoeBrand</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input onChange={handleShoeStyle} value={shoe_style} placeholder="ShoeStyle" required type="text" name="ShoeStyle" id="starts" className="form-control" />
                    <label htmlFor="ShoeStyle">Shoe Style</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input onChange={handleShoeColor} value={shoe_color} placeholder="ShoeColor" required type="text" name="ShoeColor" id="ShoeColor" className="form-control" />
                    <label htmlFor="ShoeColor">Shoe Color</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input onChange={handleShoePicUrl} value={shoe_pic_url} placeholder="photo" required type="text" name="photo" id="photo" className="form-control" />
                    <label htmlFor="photo">photo</label>
                    </div>

                    <div className="mb-3">
                    <select onChange={handleBins} value={bin} id="bin" multiple={false} className="form-select">
                    <option value="">Choose a bin</option>

                    {bins.map(bin => {
                        return (
                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                        )
                    })}

                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        </>
        );

    }

export default ShoeForm
