import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom';

function ShoeList() {

    const [shoes, setShoes] = useState([])

    async function fetchShoes(){
        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes)
        }
    }
    useEffect(() => {
        fetchShoes();
    }, []);

        const deleteShoe = async (id) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${id}`
        const response = await fetch(shoeUrl, {method: "DELETE"})
        if (response.ok) {
            setShoes(shoes.filter((shoe) => shoe.id !== id));
            console.log("Deleted")
            }
        }


    return(
        <div className="container">
            <h2>All Shoes</h2>
            <Link to="/shoes/new/" className='btn btn-primary'>New Shoe</Link>

            <div className='row'>
            {shoes.map((shoe) => (
                <div key={shoe.id} className='col'>
                    <div className='card my-3 p-3 rounded'>
                        <img className='img-fluid' src={shoe.shoe_pic_url}/>

                        <h5 className="card-title ">{shoe.shoe_style}</h5>

                        <div className='card-body'>
                            <p>
                                <strong>Brand:</strong> {shoe.shoe_brand}
                            </p>
                            <p>
                                <strong>Color:</strong> {shoe.shoe_color}
                            </p>
                            <p>
                                <strong>Bin:</strong> {shoe.bin.closet_name}
                            </p>
                            <div className='d-grid'>
                            <button onClick={() => deleteShoe(shoe.id)} className="btn btn-primary" >Delete</button>

                            </div>
                        </div>

                    </div>

                </div>

            ))}
        </div>
    </div>
    )
}

export default ShoeList
