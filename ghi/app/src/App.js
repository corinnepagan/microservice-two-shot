import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import NewHat from './NewHat';import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';

function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="hats/">
            <Route index element={<HatList />} />
            <Route path="new" element={<NewHat />} />
          </Route>
          <Route path="" element={<MainPage />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes/" element={<ShoeList  />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
