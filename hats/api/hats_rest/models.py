from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

def __str__(self):
    return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"

class Hat(models.Model):

    name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200)
    picture = models.URLField(null=True)
    fabric = models.CharField(max_length=100, null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        null=True,
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.name;

    class Meta:
        ordering = ("name",)

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"id": self.id})
