from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat,  LocationVO
from common.json import ModelEncoder


class LocationVOListEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "shelf_number"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "name", "color", "fabric", "location", "picture"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "name", "color", "picture", "fabric"]

    encoders = {
        "location": LocationVOListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
                {"hats": hats},
                encoder=HatListEncoder,
                safe=False,
            )
    else:

        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id = id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.get(id = id).delete()
        return JsonResponse({"deleted": count > 0})
